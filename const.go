package ledmatrix

import "strings"

//Opcode Max72xx 操作指令碼
//Digit0~7 第幾個資料暫存器
//DECODE_MODE 七段顯示器解碼設定
//INTENSITY LED亮度設置0x0~0xF
//SCAN_LIMIT 開啟多少個資料暫存器0~7 0:1個資料暫存器 0x07:8個資料暫存器
//SHUTDOWN 啟用狀態 0:禁用 1:啟用
//DISPLAY_TEST 測試功能，開啟時全部LED燈將會亮起 0:關閉 1:開啟
type Opcode byte

const (
	NOOP Opcode = iota
	DIGIT0
	DIGIT1
	DIGIT2
	DIGIT3
	DIGIT4
	DIGIT5
	DIGIT6
	DIGIT7
	DECODE_MODE
	INTENSITY
	SCAN_LIMIT
	SHUTDOWN
	UNDEFINED0
	UNDEFINED1
	DISPLAY_TEST
)

func GetOpcode(name string) Opcode {
	name = strings.ToLower(name)
	switch name {
	case "decodemode":
		return DECODE_MODE
	case "intensity":
		return INTENSITY
	case "scanlimit":
		return SCAN_LIMIT
	case "shutdown":
		return SHUTDOWN
	case "displaytest":
		return DISPLAY_TEST
	default:
		return NOOP
	}
}

//DecodeMode 指定解碼位元數量
type DecodeMode byte

const (
	NO_DECOED    DecodeMode = 0B00000000
	ONE_DIGITS   DecodeMode = 0B00000000
	TWO_DIGITS   DecodeMode = 0B00000011
	FOUR_DIGITS  DecodeMode = 0B00001111
	EIGHT_DIGITS DecodeMode = 0B11111111
)

var registerData = map[int32]byte{
	48: 0x0, 49: 0x1, 50: 0x2, 51: 0x3,
	52: 0x4, 53: 0x5, 54: 0x6, 55: 0x7,
	56: 0x8, 57: 0x9, 45: 0xA, 69: 0xB,
	72: 0xC, 76: 0xD, 80: 0xE, 32: 0xF,
	46: 0x80,
}
