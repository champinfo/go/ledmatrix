# LedMatrix

### Matrix with MAX72xx LED driver on the Raspberry Pi

---
SPI-串列傳輸介面

樹莓派接腳

| |SPI0|SPI1|
|---|---|---|
|CE0|pin 7|pin 18|
|CE1|pin 8|pin 17|
|CE2| |pin 16|
|MISO|pin 9|pin 19|
|MOSI|pin 10|pin 20|
|SCLK|pin 11|pin 21|

若要啟動SPI1必須在/boot/config.txt設定

`` dtoverlay=spi1-3cs``

[SPI 更多資料](https://www.raspberrypi.org/documentation/hardware/raspberrypi/spi/)

