package ledmatrix

import (
	"testing"
)

func TestMatrix_Set7SegmentLed(t *testing.T) {
	type fields struct {
		Digit      []byte
		decodeMode DecodeMode
		intensity  byte
		scanLimit  byte
		shutdown   byte
	}
	type args struct {
		data string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "1",
			fields: fields{
				decodeMode: FOUR_DIGITS,
			},
			args: args{
				data: "12345678.",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Matrix{
				Digit:      tt.fields.Digit,
				decodeMode: tt.fields.decodeMode,
				intensity:  tt.fields.intensity,
				scanLimit:  tt.fields.scanLimit,
				shutdown:   tt.fields.shutdown,
			}
			m.Set7SegmentLed(tt.args.data)
		})
	}
}
