package ledmatrix

import (
	"reflect"
	time2 "time"

	"github.com/stianeikeland/go-rpio/v4"
)

var LedMatrix []*Matrix
var command = make([]byte, 0)

func DefaultMatrix() *Matrix {
	return &Matrix{
		Digit:       []byte{0, 0, 0, 0, 0, 0, 0, 0},
		decodeMode:  NO_DECOED,
		intensity:   0xF,
		scanLimit:   0x7,
		shutdown:    1,
		displayTest: 0,
	}
}

func NewMatrix(mode DecodeMode, intensity, scanLimit uint8) *Matrix {
	m := &Matrix{}
	m.Digit = []byte{0, 0, 0, 0, 0, 0, 0, 0}
	m.decodeMode = mode
	m.intensity = intensity
	m.scanLimit = scanLimit
	m.shutdown = 1
	return m
}
func AddMatrix(led ...*Matrix) {
	LedMatrix = append(LedMatrix, led...)
}

func SpiBegin(dev rpio.SpiDev) error {
	rpio.Open()

	return rpio.SpiBegin(dev)
}

func SetSpeed(hz int) {
	rpio.SpiSpeed(hz)
}

func GetDeviceCount() int {
	return len(LedMatrix)
}

func Shutdown(led *Matrix, shutdown bool) {
	if shutdown {
		led.shutdown = 0
	} else {
		led.shutdown = 1
	}
	for _, m := range LedMatrix {
		command = append(command, byte(SHUTDOWN), m.shutdown)
	}
	transmit(command...)
	command = make([]byte, 0)
}

func InitLedMatrix() {
	v := reflect.ValueOf(Matrix{})
	for i := 1; i < v.NumField(); i++ {
		for j := range LedMatrix {
			v := reflect.ValueOf(*LedMatrix[j])
			command = append(command, byte(GetOpcode(v.Type().Field(i).Name)))
			command = append(command, byte(v.Field(i).Uint()))
		}
		transmit(command...)
		command = make([]byte, 0)
	}
	ControlLed()
}

func DeviceTesting(time int) {
	for i := 0; i < time*2-1; i++ {
		for now := range LedMatrix {
			for index := range LedMatrix {
				if index == now {
					command = append(command, byte(0x0F), 0x01)
				} else {
					command = append(command, byte(0x0F), 0x00)
				}
			}
			transmit(command...)
			command = make([]byte, 0)
			time2.Sleep(500 * time2.Millisecond)
		}
	}
	for range LedMatrix {
		command = append(command, byte(0x0F), 0x00)
	}
	transmit(command...)
	command = make([]byte, 0)
}

func ControlLed() {
	for i := 0; i < 8; i++ {
		for _, m := range LedMatrix {
			command = append(command, byte(i+1))
			command = append(command, m.Digit[i])
		}
		transmit(command...)
		command = make([]byte, 0)
	}
}

func transmit(data ...byte) {
	rpio.SpiTransmit(data...)
}
