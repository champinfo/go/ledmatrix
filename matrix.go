package ledmatrix

import (
	"log"
	"math/bits"
)

//Matrix Max72XX系列晶片，記憶體資料內容 Digit0-7為各個輸出資料內容，decodeMode用於設定第幾條輸出使用7段顯示器解碼功能，
//intensity設定輸出亮度最低為0最高為15，scanLimit設定開啟幾條輸出線路最低為０(一條)最高為７(八條)，shutdown為晶片啟用狀態1為啟用0為關閉
type Matrix struct {
	Digit       []byte //0~7
	decodeMode  DecodeMode
	intensity   byte //0~F
	scanLimit   byte //0~7
	shutdown    byte
	displayTest byte
}

//SetLedData 設定LED輸出資料，通常用於不開啟7段顯示器解碼功能時資料填入用。
func (m *Matrix) SetLedData(b []byte) {
	max := int(m.scanLimit)
	b = b[:max]
	for i := range b {
		m.Digit[i] = b[i]
	}
}

//Set7SegmentLed 設定七段顯示器資料，顯示內容由0~9以及.、-、E、H、L、P、 (空白)組成。
func (m *Matrix) Set7SegmentLed(data string) {
	digits := bits.OnesCount8(byte(m.decodeMode))
	points := 0
	dataLan := 0
	for _, d := range data {
		if d == 46 {
			points++
		} else {
			dataLan++
		}
		if dataLan > digits {
			if d != 46 {
				dataLan--
			}
			break
		}
	}
	data = data[:dataLan+points]
	p := 0
	for i, d := range data {
		addr := 0
		if d == 46 {
			p++
			addr = digits - 1 - i + p
			add, _ := bits.Add(uint(m.Digit[addr]), uint(registerData[d]), 0)
			m.Digit[addr] = byte(add)
		} else {
			addr = digits - 1 - i + p
			m.Digit[addr] = registerData[d]
		}
	}
	log.Println(m.Digit)
}

//Enable 設定晶片為啟用狀態
func (m *Matrix) Enable() {
	m.shutdown = 1
}

//Disable 設定晶片為禁用狀態
func (m *Matrix) Disable() {
	m.shutdown = 0
}

//IsDisable 取得現在晶片的啟用/禁用狀態
func (m *Matrix) IsDisable() bool {
	return m.shutdown == 0
}
