package ledmatrix

import "testing"

func TestManager_InitLedMatrix(t *testing.T) {
	type fields struct {
		LedMatrix []*Matrix
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "Init",
			fields: fields{
				LedMatrix: []*Matrix{
					DefaultMatrix(), DefaultMatrix(),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			LedMatrix = tt.fields.LedMatrix
			InitLedMatrix()
		})
	}
}

func TestManager_ControlLed(t *testing.T) {
	type fields struct {
		LedMatrix []*Matrix
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "ControlLed",
			fields: fields{
				LedMatrix: []*Matrix{
					DefaultMatrix(), DefaultMatrix(),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			LedMatrix = tt.fields.LedMatrix
			ControlLed()
		})
	}
}

func TestShutdown(t *testing.T) {
	red := DefaultMatrix()
	green := DefaultMatrix()
	LedMatrix = append(LedMatrix, red, green)
	type args struct {
		led      *Matrix
		shutdown bool
	}
	tests := []struct {
		name string
		args args
	}{
		{name: "Shutdown",
			args: args{
				led:      red,
				shutdown: true,
			}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Shutdown(tt.args.led, tt.args.shutdown)
		})
	}
}
